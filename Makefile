all:
	mkdir -p bin
	cd src/appetizer/syntactic && make
	javac src/appetizer/*/*.java -classpath lib/java-cup-11a-runtime.jar:lib/gson-2.2.2.jar -d bin
clean:
	cd src/appetizer/syntactic && make clean
	rm -rf bin
